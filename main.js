const person = {
    firstName: 'Lukas',
    lastName: 'Daunoravicius',
    age: 27,
    hobbies: ['Sportas', 'Keliones', 'Filmai'],
    address: {
        street: 'Pavardenio gatve',
        city: 'Panevezys',
        
    }
}

person.email = 'lukas@gmail.com';
console.log(person)